"""MC1-P2: Optimize a portfolio.

Copyright 2018, Georgia Institute of Technology (Georgia Tech)
Atlanta, Georgia 30332
All Rights Reserved

Template code for CS 4646/7646

Georgia Tech asserts copyright ownership of this template and all derivative
works, including solutions to the projects assigned in this course. Students
and other users of this template code are advised not to share it with others
or to make it available on publicly viewable websites including repositories
such as github and gitlab.  This copyright statement should not be removed
or edited.

We do grant permission to share solutions privately with non-students such
as potential employers. However, sharing with other current or future
students of CS 7646 is prohibited and subject to being investigated as a
GT honor code violation.

-----do not edit anything above this line---

Student Name: Andrew Kim (replace with your name)
GT User ID: akim376 (replace with your User ID)
GT ID: 903389023 (replace with your GT ID)
"""


import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import numpy as np
import datetime as dt
import scipy.optimize as spo
from util import get_data, plot_data

# This is the function that will be tested by the autograder
# The student must update this code to properly implement the functionality
def optimize_portfolio(sd=dt.datetime(2008,1,1), ed=dt.datetime(2009,1,1), \
    syms=['GOOG','AAPL','GLD','XOM'], gen_plot=False):

    # Read in adjusted closing prices for given symbols, date range
    dates = pd.date_range(sd, ed)
    prices_all = get_data(syms, dates)  # automatically adds SPY
    prices = prices_all[syms]  # only portfolio symbols
    prices_SPY = prices_all['SPY']  # only SPY, for comparison later

    # find the allocations for the optimal portfolio
    count = len(syms)
    
    # Use a uniform allocation of 1/n to each of the n assets as initial guess.
    allocs = np.full(count, 1.0/count)


    # For bounds, need to pass in a sequence of 2-tuples (<low>, <high>).
    bnds = tuple((0,1)  for i in allocs )


    # For constraints, need to pass in a sequence of dicts (dictionaries)
    cons = ({ 'type': 'eq', 'fun': lambda inputs: 1.0 - np.sum(inputs) })

    min_result = spo.minimize(get_sharpe_ratio, allocs, args=(prices,), method='SLSQP', bounds=bnds, constraints=cons)

    allocs = min_result.x

    # Get daily portfolio value
    port_val = compute_port_val(allocs, prices)
    cr, adr, sddr, sr = compute_stats(allocs, prices)


    # Compare daily portfolio value with SPY using a normalized plot
    if gen_plot:
        # add code to plot here
        df_temp = pd.concat([port_val, prices_SPY], keys=['Portfolio', 'SPY'], axis=1)
        df_temp = df_temp/df_temp.ix[0,:]
        ax = df_temp.plot(title='Daily Portfolio Value and SPY', fontsize=12, x_compat= True)
        ax.xaxis.set_major_locator(mdates.MonthLocator())
        plt.xlim(xmin=sd, xmax=ed)
        ax.grid(linestyle='dashed')
        ax.set_xlabel('Date')
        ax.set_ylabel('Price')
        plt.savefig('plot.png')
        plt.gcf().clear()

    return allocs, cr, adr, sddr, sr

def compute_port_val(allocs, prices):
    normed = prices/prices.ix[0,:]
    alloced = normed * allocs
    port_val = alloced.sum(axis=1)
    return port_val


def compute_stats(allocs, prices):
    # Setting the initial values
    rfr=0.0
    sf=252.0

    port_val = compute_port_val(allocs, prices)

    cr = port_val[-1] / port_val[0] - 1

    daily_rets = port_val.copy()
    daily_rets[1:] = (port_val[1:] / port_val[:-1].values) - 1
    daily_rets.ix[0] = 0

    adr = daily_rets[1:].mean()

    sddr = daily_rets[1:].std()

    sr = np.sqrt(sf) * (daily_rets[1:] - rfr).mean() / sddr

    return cr, adr, sddr, sr


def get_sharpe_ratio(allocs, prices):
    return (-1) * compute_stats(allocs, prices)[3] #multipy -1 for max sharpe ratio

def test_code():
    # This function WILL NOT be called by the auto grader
    # Do not assume that any variables defined here are available to your function/code
    # It is only here to help you set up and test your code

    # Define input parameters
    # Note that ALL of these values will be set to different values by
    # the autograder!

    start_date = dt.datetime(2008,6,1)
    end_date = dt.datetime(2009,6,1)
    symbols = ['IBM', 'X', 'GLD', 'JPM']

    # Assess the portfolio
    allocations, cr, adr, sddr, sr = optimize_portfolio(sd = start_date, ed = end_date,\
        syms = symbols, \
        gen_plot = True)

    # Print statistics
    print "Start Date:", start_date
    print "End Date:", end_date
    print "Symbols:", symbols
    print "Allocations:", allocations
    print "Sharpe Ratio:", sr
    print "Volatility (stdev of daily returns):", sddr
    print "Average Daily Return:", adr
    print "Cumulative Return:", cr

if __name__ == "__main__":
    # This code WILL NOT be called by the auto grader
    # Do not assume that it will be called
    test_code()
