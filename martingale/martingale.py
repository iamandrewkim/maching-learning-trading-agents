"""Assess a betting strategy.  		   	  			    		  		  		    	 		 		   		 		  
  		   	  			    		  		  		    	 		 		   		 		  
Copyright 2018, Georgia Institute of Technology (Georgia Tech)  		   	  			    		  		  		    	 		 		   		 		  
Atlanta, Georgia 30332  		   	  			    		  		  		    	 		 		   		 		  
All Rights Reserved  		   	  			    		  		  		    	 		 		   		 		  
  		   	  			    		  		  		    	 		 		   		 		  
Template code for CS 4646/7646  		   	  			    		  		  		    	 		 		   		 		  
  		   	  			    		  		  		    	 		 		   		 		  
Georgia Tech asserts copyright ownership of this template and all derivative  		   	  			    		  		  		    	 		 		   		 		  
works, including solutions to the projects assigned in this course. Students  		   	  			    		  		  		    	 		 		   		 		  
and other users of this template code are advised not to share it with others  		   	  			    		  		  		    	 		 		   		 		  
or to make it available on publicly viewable websites including repositories  		   	  			    		  		  		    	 		 		   		 		  
such as github and gitlab.  This copyright statement should not be removed  		   	  			    		  		  		    	 		 		   		 		  
or edited.  		   	  			    		  		  		    	 		 		   		 		  
  		   	  			    		  		  		    	 		 		   		 		  
We do grant permission to share solutions privately with non-students such  		   	  			    		  		  		    	 		 		   		 		  
as potential employers. However, sharing with other current or future  		   	  			    		  		  		    	 		 		   		 		  
students of CS 7646 is prohibited and subject to being investigated as a  		   	  			    		  		  		    	 		 		   		 		  
GT honor code violation.  		   	  			    		  		  		    	 		 		   		 		  
  		   	  			    		  		  		    	 		 		   		 		  
-----do not edit anything above this line---  		   	  			    		  		  		    	 		 		   		 		  
  		   	  			    		  		  		    	 		 		   		 		  
Student Name: Andrew Kim (replace with your name)  		   	  			    		  		  		    	 		 		   		 		  
GT User ID: akim376 (replace with your User ID)  		   	  			    		  		  		    	 		 		   		 		  
GT ID: 903389023 (replace with your GT ID)  		   	  			    		  		  		    	 		 		   		 		  
"""  		   	  			    		  		  		    	 		 		   		 		  

import numpy as np
import matplotlib.pyplot as plt
np.set_printoptions(threshold=np.inf)		   	  			    		  		  		    	 		 		   		 		  

def author():
    return 'akim376' # replace tb34 with your Georgia Tech username.  		   	  			    		  		  		    	 		 		   		 		  

def gtid():  		   	  			    		  		  		    	 		 		   		 		  
    return 903389023 # replace with your GT ID number  		   	  			    		  		  		    	 		 		   		 		  

def get_spin_result(win_prob):
    result = False
    if np.random.random() <= win_prob:
        result = True
    return result

def simulator(win_prob):
    winnings = np.array([0])

    episode_winnings = 0
    bet_times = 1000
    while (episode_winnings < 80):
        won = False
        bet_amout = 1
        while (won == False):
            won = get_spin_result(win_prob)
            bet_times = bet_times - 1
            if (won == True):
                episode_winnings = episode_winnings + bet_amout
            else:
                episode_winnings = episode_winnings - bet_amout
                bet_amout = bet_amout * 2
            winnings = np.append(winnings, episode_winnings)
    while (bet_times > 0):
        winnings = np.append(winnings, 80)
        bet_times = bet_times - 1
    return winnings
    
def figure1(win_prob):
    """
    Figure 1: Run your simple simulator 10 times and track the winnings, starting from 0 each time. 
    Plot all 10 runs on one chart using matplotlib functions. 
    The horizontal (X) axis should range from 0 to 300, the vertical (Y) axis should range from -256 to +100. 
    Note that we will not be surprised if some of the plot lines are not visible because they exceed the vertical or horizontal scales.
    """
    plt.axis([0, 300, -256, 100])
    for f1 in range(10):
        plt.plot(simulator(win_prob))
    plt.title('Figure 1')
    plt.xlabel('Bets')
    plt.ylabel('Winning')
    plt.savefig('Figure1.png')
    plt.gcf().clear()

    
def figure2(win_prob):
    """
    Figure 2: Run your simple simulator 1000 times. Plot the mean value of winnings for each spin using the same axis bounds as Figure 1. 
    Add an additional line above and below the mean at mean+standard deviation, and mean-standard deviation of the winnings at each point.
    """
    plt.axis([0, 300, -256, 100])
    sim_result = np.zeros(shape=(1000,1001))

    for x in range(1000):
        temp_simulator = simulator(win_prob)
        for y in range(1001):
            sim_result[x][y] = temp_simulator[y]
    
    plt.plot(np.mean(sim_result, axis=0), label='mean')
    plt.plot(np.mean(sim_result, axis=0) - np.std(sim_result, axis=0), label='mean - std')
    plt.plot(np.mean(sim_result, axis=0) + np.std(sim_result, axis=0), label='mean + std')
    plt.title('Figure 2')
    plt.xlabel('Bets')
    plt.ylabel('Winning')
    plt.legend()
    plt.savefig('Figure2.png')
    plt.gcf().clear()

    
def figure3(win_prob):
    """
    Figure 3: Use the same data you used for Figure 2, but plot the median instead of the mean. 
    Be sure to include the standard deviation lines above and below the median as well.
    """
    plt.axis([0, 300, -256, 100])
    sim_result = np.zeros(shape=(1000,1001))

    for x in range(1000):
        temp_simulator = simulator(win_prob)
        for y in range(1001):
            sim_result[x][y] = temp_simulator[y]
    
    plt.plot(np.median(sim_result, axis=0), label='median')
    plt.plot(np.median(sim_result, axis=0) - np.std(sim_result, axis=0), label='median - std')
    plt.plot(np.median(sim_result, axis=0) + np.std(sim_result, axis=0), label='median + std')
    plt.title('Figure 3')
    plt.xlabel('Bets')
    plt.ylabel('Winning')
    plt.legend()
    plt.savefig('Figure3.png')
    plt.gcf().clear()
    
def realistic_simulator(win_prob):
    winnings = np.array([0])
    
    bank_roll = 256
    
    episode_winnings = 0
    bet_times = 1000
    while (episode_winnings < 80 and episode_winnings > -256):
        won = False
        bet_amout = 1
        while (won == False):
            if(bank_roll < bet_amout):
                bet_amout = bank_roll
            won = get_spin_result(win_prob)
            bet_times = bet_times - 1
            if (won == True):
                episode_winnings = episode_winnings + bet_amout
                bank_roll = bank_roll + bet_amout
            else:
                episode_winnings = episode_winnings - bet_amout
                bank_roll = bank_roll - bet_amout
                bet_amout = bet_amout * 2
            winnings = np.append(winnings, episode_winnings)
    
    while (bet_times > 0):
        if (episode_winnings >= 80):
            winnings = np.append(winnings, 80)
        else:
            winnings = np.append(winnings, -256)
        bet_times = bet_times - 1
    return winnings
    
def figure4(win_prob):
    """
    Figure 4: Run your realistic simulator 1000 times. Plot the mean value of winnings for each spin using the same axis bounds as Figure 1. 
    Add an additional line above and below the mean at mean+standard deviation, and mean-standard deviation of the winnings at each point.
    """
    plt.axis([0, 300, -256, 100])
    sim_result = np.zeros(shape=(1000,1001))

    for x in range(1000):
        temp_simulator = realistic_simulator(win_prob)
        for y in range(1001):
            sim_result[x][y] = temp_simulator[y]
    
    plt.plot(np.mean(sim_result, axis=0), label='mean')
    plt.plot(np.mean(sim_result, axis=0) - np.std(sim_result, axis=0), label='mean - std')
    plt.plot(np.mean(sim_result, axis=0) + np.std(sim_result, axis=0), label='mean + std')
    plt.title('Figure 4')
    plt.xlabel('Bets')
    plt.ylabel('Winning')
    plt.legend()
    plt.savefig('Figure4.png')
    plt.gcf().clear()

    
def figure5(win_prob):
    """
    Figure 5: Repeat the same experiment as in Figure 4, but use the median instead of the mean. 
    Be sure to include the standard deviation lines above and below the median as well.
    """
    plt.axis([0, 300, -256, 100])
    sim_result = np.zeros(shape=(1000,1001))

    for x in range(1000):
        temp_simulator = realistic_simulator(win_prob)
        for y in range(1001):
            sim_result[x][y] = temp_simulator[y]
    
    plt.plot(np.median(sim_result, axis=0), label='median')
    plt.plot(np.median(sim_result, axis=0) - np.std(sim_result, axis=0), label='median - std')
    plt.plot(np.median(sim_result, axis=0) + np.std(sim_result, axis=0), label='median + std')
    plt.title('Figure 5')
    plt.xlabel('Bets')
    plt.ylabel('Winning')
    plt.legend()
    plt.savefig('Figure5.png')
    plt.gcf().clear()
    

    
def test_code():  		   	  			    		  		  		    	 		 		   		 		  
    win_prob = 18/38.0 # set appropriately to the probability of a win  		   	  			    		  		  		    	 		 		   		 		  
    np.random.seed(gtid()) # do this only once  		   	  			    		  		  		    	 		 		   		 		  
  	

    # add your code here to implement the experiments
    
    # Experiment 1: Explore the strategy and make some charts
    figure1(win_prob)
    figure2(win_prob)
    figure3(win_prob)
    
    # Experiment 2: A more realistic gambling simulator
    figure4(win_prob)
    figure5(win_prob)

    
    
    

if __name__ == "__main__":
    test_code()	   	  			    		  		  		    	 		 		   		 		  
