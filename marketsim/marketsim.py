"""MC2-P1: Market simulator.  		   	  			    		  		  		    	 		 		   		 		  
  		   	  			    		  		  		    	 		 		   		 		  
Copyright 2018, Georgia Institute of Technology (Georgia Tech)  		   	  			    		  		  		    	 		 		   		 		  
Atlanta, Georgia 30332  		   	  			    		  		  		    	 		 		   		 		  
All Rights Reserved  		   	  			    		  		  		    	 		 		   		 		  
  		   	  			    		  		  		    	 		 		   		 		  
Template code for CS 4646/7646  		   	  			    		  		  		    	 		 		   		 		  
  		   	  			    		  		  		    	 		 		   		 		  
Georgia Tech asserts copyright ownership of this template and all derivative  		   	  			    		  		  		    	 		 		   		 		  
works, including solutions to the projects assigned in this course. Students  		   	  			    		  		  		    	 		 		   		 		  
and other users of this template code are advised not to share it with others  		   	  			    		  		  		    	 		 		   		 		  
or to make it available on publicly viewable websites including repositories  		   	  			    		  		  		    	 		 		   		 		  
such as github and gitlab.  This copyright statement should not be removed  		   	  			    		  		  		    	 		 		   		 		  
or edited.  		   	  			    		  		  		    	 		 		   		 		  
  		   	  			    		  		  		    	 		 		   		 		  
We do grant permission to share solutions privately with non-students such  		   	  			    		  		  		    	 		 		   		 		  
as potential employers. However, sharing with other current or future  		   	  			    		  		  		    	 		 		   		 		  
students of CS 7646 is prohibited and subject to being investigated as a  		   	  			    		  		  		    	 		 		   		 		  
GT honor code violation.  		   	  			    		  		  		    	 		 		   		 		  
  		   	  			    		  		  		    	 		 		   		 		  
-----do not edit anything above this line---  		   	  			    		  		  		    	 		 		   		 		  
  		   	  			    		  		  		    	 		 		   		 		  
Student Name: Andrew Kim (replace with your name)  		   	  			    		  		  		    	 		 		   		 		  
GT User ID: akim376 (replace with your User ID)  		   	  			    		  		  		    	 		 		   		 		  
GT ID: 903389023 (replace with your GT ID)  		   	  			    		  		  		    	 		 		   		 		  
"""  		   	  			    		  		  		    	 		 		   		 		  
  		   	  			    		  		  		    	 		 		   		 		  
import pandas as pd  		   	  			    		  		  		    	 		 		   		 		  
import numpy as np  		   	  			    		  		  		    	 		 		   		 		  
import datetime as dt  		   	  			    		  		  		    	 		 		   		 		  
import os  		   	  			    		  		  		    	 		 		   		 		  
from util import get_data, plot_data  	

def author():
    return 'akim376' # replace tb34 with your Georgia Tech username.	   	  			    		  		  		    	 		 		   		 		  
  		   	  			    		  		  		    	 		 		   		 		  
def compute_portvals(orders_file = "./orders/orders.csv", start_val = 1000000, commission=9.95, impact=0.005):  		   	  			    		  		  		    	 		 		   		 		  
    # this is the function the autograder will call to test your code  		   	  			    		  		  		    	 		 		   		 		  
    # NOTE: orders_file may be a string, or it may be a file object. Your  		   	  			    		  		  		    	 		 		   		 		  
    # code should work correctly with either input  		   	  			    		  		  		    	 		 		   		 		  
    # TODO: Your code here  		   	  			    		  		  		    	 		 		   		 		  
    
    # 1) Read in Orders File
    order_df = pd.read_csv(orders_file,index_col = 'Date',parse_dates=True, na_values=['nan'])
    
    # 2) Sort by dates in orders file
    order_df = order_df.sort_index()

    # 3) build data frame prices (prices should be adjusted close)
    # scan orders.csv and get all distinct symbols
    stock_symbols = order_df['Symbol'].unique().tolist()
    
    
    # use get_data and only read in trading days only
    dates = pd.date_range(order_df.index[0],order_df.index[-1])
    
    
    adj_close_prices = get_data(stock_symbols,dates)
    adj_close_prices.fillna(method = 'ffill',inplace = True)
    adj_close_prices.fillna(method = 'backfill', inplace = True)
    

    
    trading_days = pd.DataFrame(index = dates)

    # 4) create data frame trades (same structure as df_prices)
    for stock_symbol in stock_symbols:
    
    
        trades_df = order_df[order_df['Symbol'] == stock_symbol]
        
        # if you sell shares then this should be listed as a negative and buy is positive values
        trades_df.loc[trades_df.Order == 'SELL','Shares'] = trades_df.loc[trades_df.Order == "SELL",'Shares'] * -1  
        
        #create columns for share, commission, and impact
        stock_shares = stock_symbol + "_Share"
        stock_commissions = stock_symbol + "_Commission"
        stock_impacts = stock_symbol + "_Impact"
        
        trades_df = trades_df.rename(columns = {"Shares":stock_shares}) 
        trades_df[stock_commissions] = commission
        trades_df = trades_df.join(adj_close_prices[stock_symbol],how = 'left')
        
        # compute market impact from the stock trade
        trades_df[stock_impacts] = impact * trades_df[stock_shares].abs() * trades_df[stock_symbol]
        
        # drop order, symbol, and stock_symbol columns from the data frame
        trades_df.drop('Order',axis = 1,inplace = True)
        trades_df.drop('Symbol', axis = 1, inplace = True) 
        trades_df.drop(stock_symbol,axis = 1, inplace = True)
        
        trades_df = trades_df.groupby(trades_df.index).sum()
        trading_days = trading_days.join(trades_df)
 

    # fill with zeros
    trading_days = trading_days.fillna(value = 0)

    holdings_df = adj_close_prices.join(trading_days)
    
    holdings_df["holding_change"] = 0 
    
    for stock_symbol in stock_symbols:
        stock_shares = stock_symbol + "_Share"
        holdings_df["holding_change"] = holdings_df["holding_change"] - holdings_df[stock_symbol] * holdings_df[stock_shares]
        holdings_df.drop(stock_symbol,inplace = True, axis = 1)
    
    # drop SPY from data frame
    holdings_df.drop("SPY",axis = 1,inplace=True)
    
    # get the cumulative sum over the holdings_df
    holdings_df = holdings_df.cumsum(axis = 0) 
    
    # join the cumulative sum with adj_close_prices
    values_df = adj_close_prices.join(holdings_df)
    
    values_df.dropna(axis = 0)
    
    values_df["result"] = start_val + values_df["holding_change"]
    
    for stock_symbol in stock_symbols:
        stock_shares = stock_symbol + "_Share"
        stock_commissions = stock_symbol + "_Commission"
        stock_impacts = stock_symbol + "_Impact"
        values_df["result"] = values_df["result"] + values_df[stock_symbol] * values_df[stock_shares] - values_df[stock_commissions] - values_df[stock_impacts]

    portvals = values_df['result']

 		   	  			    		  		  		    	 		 		   		 		  
    return portvals  		   	  			    		  		  		    	 		 		   		 		  
  		   	  			    		  		  		    	 		 		   		 		  
def test_code():  		   	  			    		  		  		    	 		 		   		 		  
    # this is a helper function you can use to test your code  		   	  			    		  		  		    	 		 		   		 		  
    # note that during autograding his function will not be called.  		   	  			    		  		  		    	 		 		   		 		  
    # Define input parameters  		   	  			    		  		  		    	 		 		   		 		  
  		   	  			    		  		  		    	 		 		   		 		  
    of = "./orders/orders2.csv"  		   	  			    		  		  		    	 		 		   		 		  
    sv = 1000000  		   	  			    		  		  		    	 		 		   		 		  
  		   	  			    		  		  		    	 		 		   		 		  
    # Process orders  		   	  			    		  		  		    	 		 		   		 		  
    portvals = compute_portvals(orders_file = of, start_val = sv)  		   	  			    		  		  		    	 		 		   		 		  
    if isinstance(portvals, pd.DataFrame):  		   	  			    		  		  		    	 		 		   		 		  
        portvals = portvals[portvals.columns[0]] # just get the first column  		   	  			    		  		  		    	 		 		   		 		  
    else:  		   	  			    		  		  		    	 		 		   		 		  
        "warning, code did not return a DataFrame"  		   	  			    		  		  		    	 		 		   		 		  
  		   	  			    		  		  		    	 		 		   		 		  
    # Get portfolio stats  		   	  			    		  		  		    	 		 		   		 		  
    # Here we just fake the data. you should use your code from previous assignments.  		   	  			    		  		  		    	 		 		   		 		  
    start_date = dt.datetime(2008,1,1)  		   	  			    		  		  		    	 		 		   		 		  
    end_date = dt.datetime(2008,6,1)  		   	  			    		  		  		    	 		 		   		 		  
    cum_ret, avg_daily_ret, std_daily_ret, sharpe_ratio = [0.2,0.01,0.02,1.5]  		   	  			    		  		  		    	 		 		   		 		  
    cum_ret_SPY, avg_daily_ret_SPY, std_daily_ret_SPY, sharpe_ratio_SPY = [0.2,0.01,0.02,1.5]  		   	  			    		  		  		    	 		 		   		 		  
  		   	  			    		  		  		    	 		 		   		 		  
    # Compare portfolio against $SPX  		   	  			    		  		  		    	 		 		   		 		  
    print "Date Range: {} to {}".format(start_date, end_date)  		   	  			    		  		  		    	 		 		   		 		  
    print  		   	  			    		  		  		    	 		 		   		 		  
    print "Sharpe Ratio of Fund: {}".format(sharpe_ratio)  		   	  			    		  		  		    	 		 		   		 		  
    print "Sharpe Ratio of SPY : {}".format(sharpe_ratio_SPY)  		   	  			    		  		  		    	 		 		   		 		  
    print  		   	  			    		  		  		    	 		 		   		 		  
    print "Cumulative Return of Fund: {}".format(cum_ret)  		   	  			    		  		  		    	 		 		   		 		  
    print "Cumulative Return of SPY : {}".format(cum_ret_SPY)  		   	  			    		  		  		    	 		 		   		 		  
    print  		   	  			    		  		  		    	 		 		   		 		  
    print "Standard Deviation of Fund: {}".format(std_daily_ret)  		   	  			    		  		  		    	 		 		   		 		  
    print "Standard Deviation of SPY : {}".format(std_daily_ret_SPY)  		   	  			    		  		  		    	 		 		   		 		  
    print  		   	  			    		  		  		    	 		 		   		 		  
    print "Average Daily Return of Fund: {}".format(avg_daily_ret)  		   	  			    		  		  		    	 		 		   		 		  
    print "Average Daily Return of SPY : {}".format(avg_daily_ret_SPY)  		   	  			    		  		  		    	 		 		   		 		  
    print  		   	  			    		  		  		    	 		 		   		 		  
    print "Final Portfolio Value: {}".format(portvals[-1])  		   	  			    		  		  		    	 		 		   		 		  
  		   	  			    		  		  		    	 		 		   		 		  
if __name__ == "__main__":  		   	  			    		  		  		    	 		 		   		 		  
    test_code()  		   	  			    		  		  		    	 		 		   		 		  
