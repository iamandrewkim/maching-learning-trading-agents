"""
A simple wrapper for linear regression.  (c) 2015 Tucker Balch

Copyright 2018, Georgia Institute of Technology (Georgia Tech)
Atlanta, Georgia 30332
All Rights Reserved

Template code for CS 4646/7646

Georgia Tech asserts copyright ownership of this template and all derivative
works, including solutions to the projects assigned in this course. Students
and other users of this template code are advised not to share it with others
or to make it available on publicly viewable websites including repositories
such as github and gitlab.  This copyright statement should not be removed
or edited.

We do grant permission to share solutions privately with non-students such
as potential employers. However, sharing with other current or future
students of CS 7646 is prohibited and subject to being investigated as a
GT honor code violation.

-----do not edit anything above this line---
"""

import numpy as np
import math
from scipy.stats.stats import pearsonr
from scipy.stats import mode

class RTLearner(object):

    def __init__(self,leaf_size = 1, verbose = False):
        self.leaf_size = leaf_size
        self.verbose = verbose
        pass

    def author(self):
        return 'akim376' # replace tb34 with your Georgia Tech username

    #utility comparing methods
    def areSame(self,items):
        return all(x == items[0] for x in items)
    def areSmaller(self,items,target):
        return all(x <= target for x in items)
    def areBigger(self,items,target):
        return all(x > target for x in items)

    def build_tree(self,dataX,dataY):
        if dataX.shape[0] <= self.leaf_size:
            return np.array([-1,np.around(np.median(dataY)),np.nan,np.nan]).reshape(1,4)
        elif self.areSame(dataY):
            return np.array([-1,dataY[0],np.nan,np.nan]).reshape(1,4)
        else:
            row_size, col_size = dataX.shape
            best_feat_i = np.random.randint(0,col_size)
            row_pos_1 = np.random.randint(0,row_size)
            row_pos_2 = np.random.randint(0,row_size)
            split_val = (dataX[row_pos_1,best_feat_i] + dataX[row_pos_2,best_feat_i])/2.0
            if self.areSmaller(dataX[:,best_feat_i],split_val):
                return np.array([-1,np.around(np.median(dataY)),np.nan,np.nan]).reshape(1,4)
            if self.areBigger(dataX[:,best_feat_i],split_val):
                return np.array([-1,np.around(np.median(dataY)),np.nan,np.nan]).reshape(1,4)

            #build left and right tree
            lefttree = self.build_tree(dataX[ dataX[:,best_feat_i] <= split_val ], dataY[ dataX[:,best_feat_i] <= split_val ])
            righttree = self.build_tree(dataX[ dataX[:,best_feat_i] > split_val ], dataY[ dataX[:,best_feat_i] > split_val ])
            root = np.array([best_feat_i,split_val,1,lefttree.shape[0]+1])


            wholetree = root
            wholetree = np.vstack([wholetree,lefttree])
            wholetree = np.vstack([wholetree,righttree])
            return wholetree


    def addEvidence(self,dataX,dataY):
        self.result = self.build_tree(dataX,dataY)
        return self.result


    def search_tree(self,target,result,index):
        if int(result[index,0]) == -1:
            return result[index,1]
        if target[int(result[index,0])] <= result[index,1]:
            return self.search_tree(target, result, int(index+1))
        if target[int(result[index,0])] > result[index,1]:
            return self.search_tree(target, result, int(index + result[index,3]) )

    def query(self,points):
        num_row = points.shape[0]
        estimated_values = []
        for i in range(0,num_row):
            estimated_values = np.append(estimated_values,self.search_tree(points[i,:],self.result,0))
        return estimated_values


if __name__=="__main__":
    print "the secret clue is 'zzyzx'"
