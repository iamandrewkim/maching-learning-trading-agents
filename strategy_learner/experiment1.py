import numpy as np
import pandas as pd
from util import get_data, plot_data
import datetime as dt
import indicators as ind
import ManualStrategy as manst
import marketsimcode as mksim
import StrategyLearner as sl 
import datetime as dt
import matplotlib.pyplot as plt
import util as ut
import sys

def author():
    return 'akim376' # replace tb34 with your Georgia Tech username


def experiment1(df_trades, symbol = "JPM", sd=dt.datetime(2010,1,1), ed=dt.datetime(2011,12,31), sv = 100000, impact=0):
    symb=[symbol]
    dates = pd.date_range(sd, ed)
    prices = ut.get_data(symb, dates)

    orders = []
    num_trades = df_trades.size

    for i in range(num_trades):
        if df_trades.ix[i,0] == 1000:
            orders.append([df_trades.index[i], symbol, 'BUY', 1000])
        if df_trades.ix[i,0] == -1000:
            orders.append([df_trades.index[i], symbol, 'SELL', 1000])
        if df_trades.ix[i,0] == 2000:
            orders.append([df_trades.index[i], symbol, 'BUY', 2000])
        if df_trades.ix[i,0] == -2000:
            orders.append([df_trades.index[i], symbol, 'SELL', 2000])

            
    df_orders = pd.DataFrame(orders)
    df_orders.columns = ['Date', 'Symbol', 'Order','Shares']
    buying_df = df_orders[df_orders['Order'] == 'BUY']
    selling_df = df_orders[df_orders['Order'] == 'SELL']
    buying_df = buying_df.set_index('Date')
    selling_df = selling_df.set_index('Date')
    
    prices_df,orders_df, dates,symbol_list = mksim.get_trade_info(df_orders, sd, ed)
    
    df_trades = mksim.get_trades(orders_df, prices_df, dates, symbol_list, 0, impact)
    portvals = mksim.compute_portvals(df_orders,sv,0,impact,sd,ed)
    
    benchmark_orders = []
    benchmark_orders.append([prices.index[0], 'JPM', 'BUY', 1000])

    benchmark_df = pd.DataFrame(benchmark_orders)

    benchmark_df.columns = ['Date', 'Symbol', 'Order','Shares']
    benchmark_portval = mksim.compute_portvals(benchmark_df, sv, 0,impact, sd, ed)
    
    portvals.columns = ['Date','JPM']
    benchmark_portval.columns = ['Date','Benchmark']


    df_portvals_plot = portvals/(sv * 1.0)
    benchmark_plot = benchmark_portval/(sv * 1.0)

    ax = df_portvals_plot.plot(legend = True, label = "JPM", color = 'black')
    benchmark_plot.plot(ax = ax, legend = True, label = "Benchmark", color = 'blue')

    plt.vlines(buying_df.index,0,2,  color='green')
    plt.vlines(selling_df.index,0,2,  color = 'red')

    plt.title("Strategy Learner")
    plt.savefig("Figure_experiment1.png")
    plt.close()


    day_ret_benchmark = (benchmark_portval/benchmark_portval.shift(1)) - 1
    day_ret_benchmark.ix[0] = 0.0

    day_ret__portvals = (portvals/portvals.shift(1)) - 1
    day_ret__portvals.ix[0] = 0.0

    cum_ret_benchmark = benchmark_portval[-1]/benchmark_portval[0] - 1
    cum_ret_portvals = portvals[-1]/portvals[0] - 1

    mean_benchmark = day_ret_benchmark.mean()
    mean_portvals = day_ret__portvals.mean()

    std_benchmark = day_ret_benchmark.std()
    std_portvals = day_ret__portvals.std()
    print "------------------------------------------------------------------------"
    print "JPM Data"
    print "JPM cumulative return: %f" % (cum_ret_portvals)
    print "JPM mean daily return: %f" % (mean_portvals)
    print "JPM standard deviation of daily return: %f" % (std_portvals)
    print "------------------------------------------------------------------------"
    print "benchmark Data"
    print "benchmark cumulative return: %f" % (cum_ret_benchmark)
    print "benchmark mean daily return: %f" % (mean_benchmark)
    print "benchmark standard deviation of daily return: %f" % (std_benchmark)
    print "------------------------------------------------------------------------"


    


    return df_trades




if __name__=="__main__":
    print "Strategy Learner"
    slearner = sl.StrategyLearner(impact=0)
    slearner.addEvidence(symbol = "JPM", sd=dt.datetime(2008,1,1), ed=dt.datetime(2009,12,31), sv = 100000)
    df_trades = slearner.testPolicy(symbol = "JPM", sd=dt.datetime(2010,1,1), ed=dt.datetime(2011,12,31), sv = 100000)
    experiment1(df_trades=df_trades, symbol = "JPM", sd=dt.datetime(2010,1,1), ed=dt.datetime(2011,12,31), sv = 100000, impact = 0)
    print "------------------------------------------------------------------------"
    print "Manual Strategy"
    manst.testPolicy(symbol = "JPM",sd=dt.datetime(2010,1,1), ed=dt.datetime(2011,12,31), sv = 100000, impact = 0)
    