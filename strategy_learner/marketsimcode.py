"""
Template for implementing marketsimcode  (c) 2016 Tucker Balch

Copyright 2018, Georgia Institute of Technology (Georgia Tech)
Atlanta, Georgia 30332
All Rights Reserved

Template code for CS 4646/7646

Georgia Tech asserts copyright ownership of this template and all derivative
works, including solutions to the projects assigned in this course. Students
and other users of this template code are advised not to share it with others
or to make it available on publicly viewable websites including repositories
such as github and gitlab.  This copyright statement should not be removed
or edited.

We do grant permission to share solutions privately with non-students such
as potential employers. However, sharing with other current or future
students of CS 7646 is prohibited and subject to being investigated as a
GT honor code violation.

-----do not edit anything above this line---

Student Name: Andrew Kim (replace with your name)
GT User ID: akim376 (replace with your User ID)
GT ID: 903389023 (replace with your GT ID)
"""

import numpy as np
import pandas as pd
from util import get_data, plot_data
import datetime as dt

def author():
    return 'akim376' # replace tb34 with your Georgia Tech username

    #use in manual strategy
def compute_portvals(orders_df, start_val = 1000000, commission=0.0, impact=0.005, sd=dt.datetime(2008,1,1), ed=dt.datetime(2009,12,31) ):
    prices_df,df_orders,dates,symbol_list = get_trade_info(orders_df, sd, ed)
    trades_df = get_trades(df_orders, prices_df, dates, symbol_list, commission, impact)
    holding_df = get_holdings(trades_df, start_val, dates, symbol_list)
    values_df = get_values(holding_df, prices_df, dates, symbol_list)
    portvals = values_df.sum(axis = 1)
    return portvals



def get_values(df_holding, prices_df, dates, symbol_list):
    values_df = pd.DataFrame(data= {'Date':dates})
    for symb in symbol_list:
          values_df[symb] = 0.0
    values_df['CASH'] = df_holding['CASH'].values
    values_df = values_df.set_index(['Date'])
    array_date = values_df.index
    for date in array_date:
        date = str(date).split(' ')[0]
        values_df.ix[date][:-1] = prices_df.ix[date][:-1] * df_holding.ix[date][:-1]
    return values_df


def get_holdings(trades_df, start_val, dates, symbol_list):

    df_holding = pd.DataFrame(data= {'Date':dates})
    for symb in symbol_list:
        df_holding[symb] = 0.0
    df_holding['CASH'] = 0.0
    df_holding = df_holding.set_index(['Date'])
    df_holding.ix[0]['CASH'] = float(start_val)

    row,col = trades_df.shape
    for row_index in range(row):
        df_holding.ix[row_index] = df_holding.ix[row_index] + trades_df.ix[row_index]
        if row_index > 0:
            df_holding.ix[row_index] = df_holding.ix[row_index] + df_holding.ix[row_index-1] 

    return df_holding

#use in manual strategy
def get_trades(orders_df, prices_df, dates, symbol_list, commission, impact):
    trades_df = pd.DataFrame(data= {'Date':dates})
    for symb in symbol_list:
        trades_df[symb] = 0.0
    trades_df['CASH'] = 0.0
    trades_df = trades_df.set_index(['Date'])
    date_arr = trades_df.index

    for date in date_arr:
        if date in orders_df.index:
            date = str(date).split(' ')[0]
            day_order = orders_df.ix[date]
            day_order_size = day_order.shape 

            if len(day_order_size) == 1:
                size = 1
            else:
                size = day_order_size[0]
                
            symbols = day_order['Symbol']
            orders = day_order['Order']
            shares = day_order['Shares']

            if size == 1:
                if orders == 'BUY':
                    sign = 1
                if orders== 'SELL':
                    sign = -1

                trades_df.ix[date][symbols] = trades_df.ix[date][symbols] + sign * float(shares)
                trades_df.ix[date]['CASH'] = -1 * sign * float(shares) * float(prices_df.ix[date][symbols]) * (1+ sign * impact) - commission

            else:
                for index in range(size):
                    if orders[index] == 'BUY':
                        sign = 1
                    if orders[index] == 'SELL':
                        sign = -1
                    trades_df.ix[date][symbols[index]] = trades_df.ix[date][symbols[index]] + sign * float(shares[index])

                    trades_df.ix[date]['CASH'] = float(trades_df.ix[date]['CASH']) + -1 * sign * float(shares[index]) * float(prices_df.ix[date][symbols[index]]) * (1 + sign * impact) - commission

    return trades_df

#use in manual strategy
def get_trade_info(df_orders, sd, ed):
    orders_df = df_orders
    orders_df = orders_df.set_index('Date')

    symbols = orders_df['Symbol'].values
    symbol_list = list(set(symbols))

    orders_df = orders_df.sort_index(axis=0)
 
    portvals = get_data(symbol_list,pd.date_range(sd, ed))
   
    prices_df = portvals[symbol_list]
    prices_df['CASH'] = 1.0

    dates = prices_df.index
    

    return prices_df,orders_df, dates, symbol_list



