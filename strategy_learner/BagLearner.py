"""
A simple wrapper for linear regression.  (c) 2015 Tucker Balch

Copyright 2018, Georgia Institute of Technology (Georgia Tech)
Atlanta, Georgia 30332
All Rights Reserved

Template code for CS 4646/7646

Georgia Tech asserts copyright ownership of this template and all derivative
works, including solutions to the projects assigned in this course. Students
and other users of this template code are advised not to share it with others
or to make it available on publicly viewable websites including repositories
such as github and gitlab.  This copyright statement should not be removed
or edited.

We do grant permission to share solutions privately with non-students such
as potential employers. However, sharing with other current or future
students of CS 7646 is prohibited and subject to being investigated as a
GT honor code violation.

-----do not edit anything above this line---
"""

import numpy as np
#import DTLearner as dt
import RTLearner as rt
#import BagLearner as bg
#import LinRegLearner as lrl
import math
from scipy.stats.stats import pearsonr
from scipy.stats import mode


class BagLearner(object):

    def __init__(self,learner = rt.RTLearner, kwargs ={}, bags = 10, boost = False, verbose = False):
        self.learner = []
        self.bags = bags
        self.boost = boost
        self.verbose = verbose
        for bag in range(0,self.bags):
            self.learner.append(learner(**kwargs))

    def author(self):
        return 'akim376' # replace tb34 with your Georgia Tech username


    def addEvidence(self,dataX,dataY):
        num_sample = dataX.shape[0]
        for i in range(0,self.bags):
            index = np.random.randint(0,num_sample,size=num_sample)
            self.learner[i].addEvidence(dataX[index,:],dataY[index])

    def query(self,points):
        estimated_values = []
        for i in range(0,self.bags):
            estimated_values.append(self.learner[i].query(points))
        return mode(estimated_values,axis=0)[0][0]

if __name__=="__main__":
    print "the secret clue is 'zzyzx'"

