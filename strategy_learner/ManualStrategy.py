"""
Template for implementing ManualStrategy  (c) 2016 Tucker Balch

Copyright 2018, Georgia Institute of Technology (Georgia Tech)
Atlanta, Georgia 30332
All Rights Reserved

Template code for CS 4646/7646

Georgia Tech asserts copyright ownership of this template and all derivative
works, including solutions to the projects assigned in this course. Students
and other users of this template code are advised not to share it with others
or to make it available on publicly viewable websites including repositories
such as github and gitlab.  This copyright statement should not be removed
or edited.

We do grant permission to share solutions privately with non-students such
as potential employers. However, sharing with other current or future
students of CS 7646 is prohibited and subject to being investigated as a
GT honor code violation.

-----do not edit anything above this line---

Student Name: Andrew Kim (replace with your name)
GT User ID: akim376 (replace with your User ID)
GT ID: 903389023 (replace with your GT ID)
"""

import matplotlib
matplotlib.use('Agg')
import numpy as np
import pandas as pd
from util import get_data, plot_data
import datetime as dt
import marketsimcode as mksim
import matplotlib.pyplot as plt
import indicators as ind


def author():
    return 'akim376' # replace tb34 with your Georgia Tech username

def testPolicy(symbol = "JPM", sd=dt.datetime(2010,1,1), ed=dt.datetime(2011,12,31), sv = 100000, impact = 0):
    filename = []
    filename.append(symbol)
    price = get_data(filename,pd.date_range(sd, ed))
    num_rows = price.shape[0]
    orders = []
    buying = False
    sma = ind.get_sma(price, 20, symbol)
    
    for today in range(21, price.shape[0]):
        if ( (sma.ix[today,1] < 0.95) ):
            orders.append([price.index[today], symbol, 'BUY', 1000])
            buying = True
            break
        if ( (sma.ix[today,1] > 1.05) ):
            orders.append([price.index[today], symbol, 'SELL', 1000])
            buying = False
            break
    
    for tomorrow in range(today+1, price.shape[0]):
        if ( (buying == False) and (sma.ix[tomorrow,1] < 0.95) ):
            orders.append([price.index[tomorrow], symbol, 'BUY', 2000])
            buying = True
        if ( (buying == True) and (sma.ix[tomorrow,1] > 1.05) ):
            orders.append([price.index[tomorrow], symbol, 'SELL', 2000])
            buying = False

    df_orders = pd.DataFrame(orders)

    df_orders.columns = ['Date', 'Symbol', 'Order','Shares']
    buying_df = df_orders[df_orders['Order'] == 'BUY']
    selling_df = df_orders[df_orders['Order'] == 'SELL']
    buying_df = buying_df.set_index('Date')
    selling_df = selling_df.set_index('Date')
    
    prices_df,orders_df, dates,symbol_list = mksim.get_trade_info(df_orders, sd, ed)
    
    df_trades = mksim.get_trades(orders_df, prices_df, dates, symbol_list, 0, impact)
    portvals = mksim.compute_portvals(df_orders,sv,0,impact,sd,ed)
    
    benchmark_orders = []
    benchmark_orders.append([price.index[0], 'JPM', 'BUY', 1000])

    benchmark_df = pd.DataFrame(benchmark_orders)

    benchmark_df.columns = ['Date', 'Symbol', 'Order','Shares']
    benchmark_portval = mksim.compute_portvals(benchmark_df, sv, 0,impact, sd, ed)
    
    portvals.columns = ['Date','JPM']
    benchmark_portval.columns = ['Date','Benchmark']


    df_portvals_plot = portvals/(sv * 1.0)
    benchmark_plot = benchmark_portval/(sv * 1.0)

    ax = df_portvals_plot.plot(legend = True, label = "JPM", color = 'black')
    benchmark_plot.plot(ax = ax, legend = True, label = "Benchmark", color = 'blue')

    plt.vlines(buying_df.index,0,2,  color='green')
    plt.vlines(selling_df.index,0,2,  color = 'red')

    plt.title("Manual Strategy")
    #plt.show()
    plt.savefig('Figure_ms.png')
    plt.close()


    day_ret_benchmark = (benchmark_portval/benchmark_portval.shift(1)) - 1
    day_ret_benchmark.ix[0] = 0.0

    day_ret__portvals = (portvals/portvals.shift(1)) - 1
    day_ret__portvals.ix[0] = 0.0

    cum_ret_benchmark = benchmark_portval[-1]/benchmark_portval[0] - 1
    cum_ret_portvals = portvals[-1]/portvals[0] - 1

    mean_benchmark = day_ret_benchmark.mean()
    mean_portvals = day_ret__portvals.mean()

    std_benchmark = day_ret_benchmark.std()
    std_portvals = day_ret__portvals.std()

    print "------------------------------------------------------------------------"
    print "JPM Data"
    print "JPM cumulative return: %f" % (cum_ret_portvals)
    print "JPM mean daily return: %f" % (mean_portvals)
    print "JPM standard deviation of daily return: %f" % (std_portvals)
    print "------------------------------------------------------------------------"
    print "benchmark Data"
    print "benchmark cumulative return: %f" % (cum_ret_benchmark)
    print "benchmark mean daily return: %f" % (mean_benchmark)
    print "benchmark standard deviation of daily return: %f" % (std_benchmark)
    print "------------------------------------------------------------------------"

   

    return df_trades


if __name__ == '__main__':
    symbol = ['JPM']
    #start_date = '2008-01-01'
    #end_date = '2009-12-31'
    start_date = '2010-01-01'
    end_date = '2011-12-31'

    df_trades = testPolicy(sd=start_date, ed=end_date)








