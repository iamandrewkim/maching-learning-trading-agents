"""
Template for implementing StrategyLearner  (c) 2016 Tucker Balch

Copyright 2018, Georgia Institute of Technology (Georgia Tech)
Atlanta, Georgia 30332
All Rights Reserved

Template code for CS 4646/7646

Georgia Tech asserts copyright ownership of this template and all derivative
works, including solutions to the projects assigned in this course. Students
and other users of this template code are advised not to share it with others
or to make it available on publicly viewable websites including repositories
such as github and gitlab.  This copyright statement should not be removed
or edited.

We do grant permission to share solutions privately with non-students such
as potential employers. However, sharing with other current or future
students of CS 7646 is prohibited and subject to being investigated as a
GT honor code violation.

-----do not edit anything above this line---

Student Name: Andrew Kim (replace with your name)
GT User ID: akim376 (replace with your User ID)
GT ID: 903389023 (replace with your GT ID)
"""

import datetime as dt
import pandas as pd
import util as ut
import BagLearner as bl
import RTLearner as rt
import random
import indicators as ind
import numpy as np

class StrategyLearner(object):

    def author(self):
        return 'akim376' # replace tb34 with your Georgia Tech username.

    # constructor
    def __init__(self, verbose = False, impact=0.0):
        self.verbose = verbose
        self.impact = impact

    # this method should create a QLearner, and train it for trading
    def addEvidence(self, symbol = "IBM", sd=dt.datetime(2008,1,1), ed=dt.datetime(2009,12,31), sv = 10000):

        # add your code to do learning here
        # example usage of the old backward compatible util function
        syms=[symbol]
        dates = pd.date_range(sd, ed)
        prices_all = ut.get_data(syms, dates)  # automatically adds SPY
        prices = prices_all[syms]  # only portfolio symbols
        prices_SPY = prices_all['SPY']  # only SPY, for comparison later

        # set lookback 20
        sma = ind.get_sma(prices, 20, symbol)
        dataX = np.asarray(sma.values)

        
        YBUY = 0.05 + self.impact
        YSELL = -0.05 - self.impact
        N = 10
        
        daily_rets = prices.shift(-N)/prices - 1.0
        num_rows = daily_rets.size

        #zero initial Y values
        dataY = np.zeros(num_rows, dtype=float)


        for i in range(num_rows):
            if daily_rets.values[i] > YBUY:
                dataY[i] = 1.0
            elif daily_rets.values[i] < YSELL:
                dataY[i] = -1.0
            else:
                dataY[i] = 0.0

        global learner
        learner = bl.BagLearner(learner = rt.RTLearner , kwargs = {"leaf_size":5}, bags = 20, boost = False, verbose = False)
        learner.addEvidence(dataX, dataY)

        volume_all = ut.get_data(syms, dates, colname = "Volume")  # automatically adds SPY
        volume = volume_all[syms]  # only portfolio symbols
        volume_SPY = volume_all['SPY']  # only SPY, for comparison later


    # this method should use the existing policy and test it against new data
    def testPolicy(self, symbol = "JPM", sd=dt.datetime(2010,1,1), ed=dt.datetime(2011,12,31), sv = 10000):

        # here we build a fake set of trades
        # your code should return the same sort of date
        dates = pd.date_range(sd, ed)
        prices_all = ut.get_data([symbol], dates)  # automatically adds SPY
        trades = prices_all[[symbol,]]  # only portfolio symbols
        prices = trades
        trades_SPY = prices_all['SPY']  # only SPY, for comparison later

        sma = ind.get_sma(prices, 20, symbol)
        train_X = np.asarray(sma.values)

        action = learner.query(train_X)

        buying = False

        action_size = action.size
        trades.values[:,:] = 0


        for today in range(action_size):
            if action[today] == -1.0:
                trades.values[today,:] = -1000
                buying = False
                break
            if action[today] == 1.0:
                trades.values[today,:] = 1000
                buying = True
                break

        #to beat benchmark
        for tomorrow in range(today + 1, action_size):
            if (buying == False) and (action[tomorrow] == 1.0):
                trades.values[tomorrow,:] = 2000
                buying = True
            if (buying == True) and (action[tomorrow] == -1.0):
                trades.values[tomorrow,:] = -2000
                buying = False

        return trades

if __name__=="__main__":
    print "One does not simply think up a strategy"
