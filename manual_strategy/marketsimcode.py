"""MC2-P1: Market simulator.  		   	  			    		  		  		    	 		 		   		 		  
  		   	  			    		  		  		    	 		 		   		 		  
Copyright 2018, Georgia Institute of Technology (Georgia Tech)  		   	  			    		  		  		    	 		 		   		 		  
Atlanta, Georgia 30332  		   	  			    		  		  		    	 		 		   		 		  
All Rights Reserved  		   	  			    		  		  		    	 		 		   		 		  
  		   	  			    		  		  		    	 		 		   		 		  
Template code for CS 4646/7646  		   	  			    		  		  		    	 		 		   		 		  
  		   	  			    		  		  		    	 		 		   		 		  
Georgia Tech asserts copyright ownership of this template and all derivative  		   	  			    		  		  		    	 		 		   		 		  
works, including solutions to the projects assigned in this course. Students  		   	  			    		  		  		    	 		 		   		 		  
and other users of this template code are advised not to share it with others  		   	  			    		  		  		    	 		 		   		 		  
or to make it available on publicly viewable websites including repositories  		   	  			    		  		  		    	 		 		   		 		  
such as github and gitlab.  This copyright statement should not be removed  		   	  			    		  		  		    	 		 		   		 		  
or edited.  		   	  			    		  		  		    	 		 		   		 		  
  		   	  			    		  		  		    	 		 		   		 		  
We do grant permission to share solutions privately with non-students such  		   	  			    		  		  		    	 		 		   		 		  
as potential employers. However, sharing with other current or future  		   	  			    		  		  		    	 		 		   		 		  
students of CS 7646 is prohibited and subject to being investigated as a  		   	  			    		  		  		    	 		 		   		 		  
GT honor code violation.  		   	  			    		  		  		    	 		 		   		 		  
  		   	  			    		  		  		    	 		 		   		 		  
-----do not edit anything above this line---  		   	  			    		  		  		    	 		 		   		 		  
  		   	  			    		  		  		    	 		 		   		 		  
Student Name: Andrew Kim (replace with your name)  		   	  			    		  		  		    	 		 		   		 		  
GT User ID: akim376 (replace with your User ID)  		   	  			    		  		  		    	 		 		   		 		  
GT ID: 903389023 (replace with your GT ID)  		   	  			    		  		  		    	 		 		   		 		  
"""  		   	  			    		  		  		    	 		 		   		 		  
  		   	  			    		  		  		    	 		 		   		 		  
import pandas as pd  		   	  			    		  		  		    	 		 		   		 		  
import numpy as np  		   	  			    		  		  		    	 		 		   		 		  
import datetime as dt  		   	  			    		  		  		    	 		 		   		 		  
import os  		   	  			    		  		  		    	 		 		   		 		  
from util import get_data, plot_data  	

def author():
    return 'akim376' # replace tb34 with your Georgia Tech username.	   	  			    		  		  		    	 		 		   		 		  
  		   	  			    		  		  		    	 		 		   		 		  
def compute_portvals(orders_df, prices, start_val = 1000000, commission=0.00, impact=0.0):  		   	  			    		  		  		    	 		 		   		 		  

    #generate dataframe for holding and cash
    holding_df = pd.DataFrame(data=np.zeros(len(prices)), index=prices.index, columns=['val'])

    cash_df = pd.DataFrame(data=np.ones(len(prices)) * start_val, index=prices.index, columns=['val'])
    
    
    for i in range(len(prices.index)):
        purchasing_cash = 0

        if orders_df.values[i][0] > 0:
            buy = 1
        else:
            buy = -1

        #adjust cash value with impact and commission.
        if orders_df.values[i][0] != 0:
            purchasing_cash = (buy + impact) * abs(orders_df.values[i][0]) * prices.values[i][0]
            purchasing_cash = purchasing_cash + commission
        
        if i == 0:
            cash_df['val'].iloc[i] = cash_df['val'].iloc[i] - purchasing_cash
            holding_df['val'].iloc[i] = holding_df['val'].iloc[i] + orders_df.values[i][0]
        else:
            cash_df['val'].iloc[i] = cash_df['val'].iloc[i - 1] - purchasing_cash
            holding_df['val'].iloc[i] = holding_df['val'].iloc[i - 1] + orders_df.values[i][0]

   
    portvals = pd.np.multiply(holding_df, prices) + cash_df
    return portvals 		   	  			    		  		  		    	 		 		   		 		  
  		   	  			    		  		  		    	 		 		   		 		  
		   	  			    		  		  		    	 		 		   		 		  
  		   	  			    		  		  		    	 		 		   		 		 	   	  			    		  		  		    	 		 		   		 		  
