import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import math
import datetime as dt
import types
import os
from util import get_data, plot_data

def author():
    return 'akim376'

#get rolling_mean
def get_rolling_mean(values, window = 20):
    rolling_mean = np.cumsum(values, dtype = float)
    rolling_mean[window :] = rolling_mean[window :] - rolling_mean[: -window]
    return rolling_mean[window - 1 : -1] / window
  
    
#get simple moving average
def get_sma(prices, window = 20):
    #normalize prices
    prices = prices / prices.ix[0]
    prices_values = np.array(prices.values)
   
    
    rolling_mean = np.concatenate((np.array([np.nan] * (window)), get_rolling_mean(prices_values, window = window)))
    df_rolling_mean = pd.DataFrame(data=rolling_mean, index=prices.index, columns=['val'])
    indicator_sma = prices_values / rolling_mean - 1

    df_indicator = pd.DataFrame(data=indicator_sma, index=prices.index, columns=['val'])
    
    pr, = plt.plot(prices)
    sm, = plt.plot(df_rolling_mean)
    ind, = plt.plot(df_indicator)
    #plt.legend([sm, pr], ['Simple Moving Average', 'Prices'])
    
    plt.legend([sm, pr, ind], ['Simple Moving Average', 'Prices', 'Prices/SMA - 1'])
    plt.title('Technical Indicator: Simple Moving Average')
    plt.xlabel('Dates')
    plt.ylabel('Normalized Data')
    #plt.savefig('Figure1.png')
    plt.show()
    plt.gcf().clear()
    return indicator_sma

#get bollinger band
def get_bb(prices, window = 20):
    ind = [0] * len(prices.index)
    #normalize prices
    prices = prices / prices.ix[0]
    prices_values = np.array(prices.values)
    rolling_mean = np.concatenate((np.array([np.nan] * (window)), get_rolling_mean(prices_values, window = window)))
    rolling_std = np.array([np.nan] * window + [prices_values[start : start + window].std() for start in range(len(ind) - window)])

    df_rolling_mean = pd.DataFrame(data=rolling_mean, index=prices.index, columns=['val'])
    df_rolling_std = pd.DataFrame(data=rolling_std, index=prices.index, columns=['val'])



    indicator_bb = (prices_values - rolling_mean) / (rolling_std * 2)
    df_indicator = pd.DataFrame(data=indicator_bb, index=prices.index, columns=['val'])

    pr, = plt.plot(prices)
    rm, = plt.plot(df_rolling_mean)
    up, = plt.plot(df_rolling_mean + 2 * df_rolling_std, 'k')
    down, = plt.plot(df_rolling_mean - 2 * df_rolling_std, 'k')
    ind, = plt.plot(df_indicator)
    
    plt.legend([rm, pr, up, ind], ['Rolling Mean', 'Prices', 'Bollinger Band', 'Indicator'])
    plt.title('Technical Indicator: Bollinger Band')
    plt.xlabel('Dates')
    plt.ylabel('Normalized Data')
    #plt.savefig('Figure2.png')
    plt.show()
    plt.gcf().clear()
    return indicator_bb

    #used algorithm from https://www.learndatasci.com/tutorials/python-finance-part-3-moving-average-trading-strategy/
    #get exponential moving average
def get_ema(prices, window = 20):

    prices = prices / prices.ix[0]
    prices_values = np.array(prices.values)
    
    
    decay_parameter = 2.0 / (window + 1)

    #fill with zeros
    ema = len(prices) * [0]

    ema[0] = prices[0]
   

    for i in range(1, len(prices)):
        ema[i] = (1 - decay_parameter) * ema[i - 1]+ decay_parameter * prices[i]
    
    ema = np.array(ema)

    df_ema = pd.DataFrame(data=ema, index=prices.index, columns=['val'])
    indicator_ema = prices_values / ema - 1
    df_indicator = pd.DataFrame(data=indicator_ema, index=prices.index, columns=['val'])

    pr, = plt.plot(prices)
    em, = plt.plot(df_ema)
    ind, = plt.plot(df_indicator)
    plt.legend([em, pr, ind], ['Exponential Moving Average', 'Price', 'Prices/EMA - 1'])
    plt.title('Technical Indicator: Exponential Moving Average')
    plt.xlabel('Dates')
    plt.ylabel('Normalized Data')
    #plt.savefig('Figure3.png')
    plt.show()
    plt.gcf().clear()
    return indicator_ema


if __name__ == '__main__':
    symbol = 'JPM'
    start_date = '2008-01-01'
    end_date = '2009-12-31'
    prices = get_data([symbol], pd.date_range(start_date, end_date))
    prices = prices[symbol]
    
    #get_sma(prices)
    get_bb(prices)
    #get_ema(prices)
