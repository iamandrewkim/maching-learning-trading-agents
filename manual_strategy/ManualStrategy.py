import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import math
import datetime as dt
import types
import os
from util import get_data, plot_data
from marketsimcode import compute_portvals
from indicators import get_sma, get_bb, get_ema


def author():
    return 'akim376' # replace tb34 with your Georgia Tech username.	   

def report_output(portvals):
    portvals = portvals / portvals.ix[0]
    daily_returns = portvals / portvals.shift(1) - 1
    daily_returns = daily_returns[1:]

    print 'Cumulative return: ' + str(float(portvals.values[-1] / portvals.values[0]) - 1)
    print 'Mean of daily returns: ' + str(float(daily_returns.mean()))
    print 'Stdev of daily returns: ' + str(float(daily_returns.std()))

def testPolicy(symbol = 'JPM', sd = dt.datetime(2008, 1, 1), ed = dt.datetime(2009, 12, 31), sv = 100000):
    prices = get_data([symbol], pd.date_range(sd, ed))
    prices = prices[symbol]
    df_trades = pd.DataFrame(data=np.zeros(len(prices.index)), index=prices.index, columns = ['val'])

    orders = 0
    my_window = 20

    sma_value = get_sma(prices, window = my_window)
    #bb_value = get_bb(prices, window = my_window)
    #ema_value = get_ema(prices, window = my_window)

    #using sma alone for manual strategy instead of combining
    for i in range(my_window, len(prices.index)):
        #buy
        if sma_value[i] < -0.05:
            df_trades['val'].iloc[i] = 1000 - orders
            orders = 1000
        #sell
        elif sma_value[i] > 0.05:
            df_trades['val'].iloc[i] = - orders - 1000
            orders = -1000

    return df_trades

if __name__ == '__main__':
    symbol = ['JPM']
    start_date = '2008-01-01'
    end_date = '2009-12-31'
    #start_date = '2010-01-01'
    #end_date = '2011-12-31'
    prices = get_data(symbol, pd.date_range(start_date, end_date))
    prices = prices[symbol]

    df_trades = testPolicy(sd=start_date, ed=end_date)
    

    portvals = compute_portvals(df_trades, prices, commission = 9.95, impact = 0.005)

    
    prices_val = prices.values


    benchmark_data = np.zeros(len(prices.index))
    benchmark_data[0] = 1000
    df_trade_benchmark = pd.DataFrame(data=benchmark_data, index=prices.index, columns = ['val'])
    portvals_benchmark = compute_portvals(df_trade_benchmark, prices, commission = 9.95, impact = 0.005)

    portvals = portvals / portvals.ix[0]
    portvals_benchmark = portvals_benchmark / portvals_benchmark.ix[0]

    print 'Manual Strategy'
    report_output(portvals)
    print 'Benchmark'
    report_output(portvals_benchmark)

    benchmark, = plt.plot(portvals_benchmark, 'b')
    man_strategy, = plt.plot(portvals, 'k')
    for i in range(len(prices.index)):
        if df_trades['val'].iloc[i] > 0:
            plt.axvline(x=prices.index[i], c = 'g')
        elif df_trades['val'].iloc[i] < 0:
            plt.axvline(x=prices.index[i], c = 'r')

    plt.legend([benchmark, man_strategy], ['Benchmark', 'Manual Strategy'])
    plt.title('Manual Strategy')
    plt.xlabel('Dates')
    plt.ylabel('Normalized Data')
    plt.show()