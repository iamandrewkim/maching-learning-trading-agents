import pandas as pd
import numpy as np
import math
import datetime as dt
import types
import os
import matplotlib.pyplot as plt
from util import get_data, plot_data
from marketsimcode import compute_portvals


def author():
    return 'akim376' # replace tb34 with your Georgia Tech username.	   

def testPolicy(symbol = 'JPM', sd = dt.datetime(2008, 1, 1), ed = dt.datetime(2009, 12, 31), sv = 100000):
    prices = get_data([symbol], pd.date_range(sd, ed))
    prices = prices[symbol]
    df_trades = pd.DataFrame(data=np.zeros(len(prices.index)), index=prices.index, columns = ['val'])
    prices_values = prices.values


    going_up = prices_values[1] > prices_values[0]
    if going_up:
        df_trades['val'].iloc[0] = 1000 * 1
    else:
        df_trades['val'].iloc[0] = 1000 * -1

    for i in range(1, len(prices_values) - 1):
        if (prices_values[i] < prices_values[i + 1] and not going_up) or (prices_values[i] >= prices_values[i + 1] and going_up):
            going_up = not going_up
            if going_up:
                df_trades['val'].iloc[i] = 1000 * 1
            else:
                df_trades['val'].iloc[i] = 1000 * -1

    return df_trades

def report_output(portvals):
    portvals = portvals / portvals.ix[0]
    daily_returns = portvals / portvals.shift(1) - 1
    daily_returns = daily_returns[1:]

    print 'Cumulative return: ' + str(float(portvals.values[-1] / portvals.values[0]) - 1)
    print 'Mean of daily returns: ' + str(float(daily_returns.mean()))
    print 'Stdev of daily returns: ' + str(float(daily_returns.std()))

if __name__ == '__main__':
    symbol = ['JPM']
    start_date = '2008-01-01'
    end_date = '2009-12-31'
    prices = get_data(symbol, pd.date_range(start_date, end_date))
    prices = prices[symbol]

    df_trades = testPolicy()
    portvals = compute_portvals(df_trades, prices)
    print 'Theoretically Optimal Strategy'
    report_output(portvals)

    benchmark_data = np.zeros(len(prices.index))
    #get 1000 shares of JPM and holding that position
    benchmark_data[0] = 1000
    df_trade_benchmark = pd.DataFrame(data=benchmark_data, index=prices.index, columns = ['val'])
    portvals_benchmark = compute_portvals(df_trade_benchmark, prices)
    print 'Benchmark'
    report_output(portvals_benchmark)

    portvals = portvals / portvals.ix[0]
    portvals_benchmark = portvals_benchmark / portvals_benchmark.ix[0]


    benchmark, = plt.plot(portvals_benchmark, 'b')
    optimal, = plt.plot(portvals, 'k')
    plt.legend([benchmark, optimal], ['Benchmark', 'Optimal'])
    plt.title('Theoretically Optimal Strategy')
    plt.xlabel('Dates')
    plt.ylabel('Normalized Data')
    plt.show()