"""  		   	  			    		  		  		    	 		 		   		 		  
Test a learner.  (c) 2015 Tucker Balch  		   	  			    		  		  		    	 		 		   		 		  
  		   	  			    		  		  		    	 		 		   		 		  
Copyright 2018, Georgia Institute of Technology (Georgia Tech)  		   	  			    		  		  		    	 		 		   		 		  
Atlanta, Georgia 30332  		   	  			    		  		  		    	 		 		   		 		  
All Rights Reserved  		   	  			    		  		  		    	 		 		   		 		  
  		   	  			    		  		  		    	 		 		   		 		  
Template code for CS 4646/7646  		   	  			    		  		  		    	 		 		   		 		  
  		   	  			    		  		  		    	 		 		   		 		  
Georgia Tech asserts copyright ownership of this template and all derivative  		   	  			    		  		  		    	 		 		   		 		  
works, including solutions to the projects assigned in this course. Students  		   	  			    		  		  		    	 		 		   		 		  
and other users of this template code are advised not to share it with others  		   	  			    		  		  		    	 		 		   		 		  
or to make it available on publicly viewable websites including repositories  		   	  			    		  		  		    	 		 		   		 		  
such as github and gitlab.  This copyright statement should not be removed  		   	  			    		  		  		    	 		 		   		 		  
or edited.  		   	  			    		  		  		    	 		 		   		 		  
  		   	  			    		  		  		    	 		 		   		 		  
We do grant permission to share solutions privately with non-students such  		   	  			    		  		  		    	 		 		   		 		  
as potential employers. However, sharing with other current or future  		   	  			    		  		  		    	 		 		   		 		  
students of CS 7646 is prohibited and subject to being investigated as a  		   	  			    		  		  		    	 		 		   		 		  
GT honor code violation.  		   	  			    		  		  		    	 		 		   		 		  
  		   	  			    		  		  		    	 		 		   		 		  
-----do not edit anything above this line---  		   	  			    		  		  		    	 		 		   		 		  
"""  		   	  			    		  		  		    	 		 		   		 		  
  		   	  			    		  		  		    	 		 		   		 		  
import numpy as np
import pandas as pd  		   
import matplotlib
matplotlib.use("Agg")	  			    		  		  		    	 		 		   		 		  
import matplotlib.pyplot as plt    		   	  			    		  		  		    	 		 		   		 		  
import math  		   	  			    		  		  		    	 		 		   		 		  
import LinRegLearner as lrl
import DTLearner as dt
import RTLearner as rt
import BagLearner as bl
import InsaneLearner as it			    		  		  		    	 		 		   		 		  
import sys  		   	  			    		  		  		    	 		 		   		 		  
  		   	  			    		  		  		    	 		 		   		 		  
if __name__=="__main__":  		   	  			    		  		  		    	 		 		   		 		  
    if len(sys.argv) != 1:  		   	  			    		  		  		    	 		 		   		 		  
        print "Usage: python testlearner.py"  		   	  			    		  		  		    	 		 		   		 		  
        sys.exit(1)  		   	  			    		  		  		    	 		 		   		 		  
    #hardcoding Data/Istanbul.csv
    
    inf = open("Data/Istanbul.csv")  		   	  			    		  		  		    	 		 		   		 		  
    data = np.genfromtxt(inf,delimiter=',')
    data = data[1:,1:]
  		   	  			    		  		  		    	 		 		   		 		  
    # compute how much of the data is training and testing  		   	  			    		  		  		    	 		 		   		 		  
    train_rows = int(0.6* data.shape[0])  		   	  			    		  		  		    	 		 		   		 		  
    test_rows = data.shape[0] - train_rows  		   	  			    		  		  		    	 		 		   		 		  
  		   	  			    		  		  		    	 		 		   		 		  
    # separate out training and testing data  		   	  			    		  		  		    	 		 		   		 		  
    trainX = data[:train_rows,0:-1]  		   	  			    		  		  		    	 		 		   		 		  
    trainY = data[:train_rows,-1]  		   	  			    		  		  		    	 		 		   		 		  
    testX = data[train_rows:,0:-1]  		   	  			    		  		  		    	 		 		   		 		  
    testY = data[train_rows:,-1]  		   	  			    		  		  		    	 		 		   		 		  
  		   	  			    		  		  		    	 		 		   		 		  
    print testX.shape  		   	  			    		  		  		    	 		 		   		 		  
    print testY.shape  		   	  			    		  		  		    	 		 		   		 		  
  		   	  			    		  		  		    	 		 		   		 		  
    # create a learner and train it  		   	  			    		  		  		    	 		 		   		 		  
    #learner = lrl.LinRegLearner(verbose = True) # create a LinRegLearner  		   	  			    		  		  		    	 		 		   		 		  
    
    row_leaf = []
    DT_insample_rmse = []
    DT_outsample_rmse = []
    
    #Generating result for question 1 in report
    for i in range(100):
        learner = dt.DTLearner(leaf_size = i, verbose = True)
        row_leaf = np.append(row_leaf, i)
        
        learner.addEvidence(trainX, trainY) # train it  
        # evaluate in sample  		   	  			    		  		  		    	 		 		   		 		  
        predY = learner.query(trainX) # get the predictions  		   	  			    		  		  		    	 		 		   		 		  
        insample_rmse = math.sqrt(((trainY - predY) ** 2).sum()/trainY.shape[0])  		   	  			    		  		  		    	 		 		   		 		  
        DT_insample_rmse = np.append(DT_insample_rmse, insample_rmse)
        predY = learner.query(testX) # get the predictions  		   	  			    		  		  		    	 		 		   		 		  
        outsample_rmse = math.sqrt(((testY - predY) ** 2).sum()/testY.shape[0])
        DT_outsample_rmse = np.append(DT_outsample_rmse, outsample_rmse)
        
    plt.plot(row_leaf, DT_insample_rmse, label = "in sample")
    plt.plot(row_leaf, DT_outsample_rmse, label = "out sample")
    plt.xlabel('Leaf Size')
    plt.ylabel('RMSE')
    plt.title('DTLearner: Leaf Size vs RMSE')
    plt.legend()
    plt.savefig('fig1.png')
    plt.gcf().clear()
    
    
    #generating result for question 2 in report
    BL_insample_rmse = []
    BL_outsample_rmse = []
    
    for i in range(100):
        learner = bl.BagLearner(learner = dt.DTLearner, kwargs = {"leaf_size": i}, bags = 20, boost = False, verbose = True)
        learner.addEvidence(trainX, trainY) # train it  
        # evaluate in sample  		   	  			    		  		  		    	 		 		   		 		  
        predY = learner.query(trainX) # get the predictions  		   	  			    		  		  		    	 		 		   		 		  
        insample_rmse = math.sqrt(((trainY - predY) ** 2).sum()/trainY.shape[0])  		   	  			    		  		  		    	 		 		   		 		  
        BL_insample_rmse = np.append(BL_insample_rmse, insample_rmse)
        predY = learner.query(testX) # get the predictions  		   	  			    		  		  		    	 		 		   		 		  
        outsample_rmse = math.sqrt(((testY - predY) ** 2).sum()/testY.shape[0])
        BL_outsample_rmse = np.append(BL_outsample_rmse, outsample_rmse)
        
    plt.plot(row_leaf, BL_insample_rmse, label = "in sample")
    plt.plot(row_leaf, BL_outsample_rmse, label = "out sample")
    plt.xlabel('Leaf Size')
    plt.ylabel('RMSE')
    plt.title('BagLearner: Leaf Size vs RMSE')
    plt.legend()
    plt.savefig('fig2.png')
    plt.gcf().clear()
    
    #generating the result for question 3 in report
    RT_insample_rmse = []
    RT_outsample_rmse = []
    
    for i in range(100):
        learner = rt.RTLearner(leaf_size = i, verbose = True)
        learner.addEvidence(trainX, trainY) # train it  
        # evaluate in sample  		   	  			    		  		  		    	 		 		   		 		  
        predY = learner.query(trainX) # get the predictions  		   	  			    		  		  		    	 		 		   		 		  
        insample_rmse = math.sqrt(((trainY - predY) ** 2).sum()/trainY.shape[0])  		   	  			    		  		  		    	 		 		   		 		  
        RT_insample_rmse = np.append(RT_insample_rmse, insample_rmse)
        predY = learner.query(testX) # get the predictions  		   	  			    		  		  		    	 		 		   		 		  
        outsample_rmse = math.sqrt(((testY - predY) ** 2).sum()/testY.shape[0])
        RT_outsample_rmse = np.append(RT_outsample_rmse, outsample_rmse)
    
    plt.plot(row_leaf, DT_insample_rmse, label = "DT in sample")    
    plt.plot(row_leaf, RT_insample_rmse, label = "RT in sample")
    plt.xlabel('Leaf Size')
    plt.ylabel('RMSE')
    plt.title('In Sample DT and RT: Leaf Size vs RMSE')
    plt.legend()
    plt.savefig('fig3.png')
    plt.gcf().clear()
    
    
    plt.plot(row_leaf, DT_outsample_rmse, label = "DT out sample")
    plt.plot(row_leaf, RT_outsample_rmse, label = "RT out sample")
    plt.xlabel('Leaf Size')
    plt.ylabel('RMSE')
    plt.title('Out Sample DT and RT: Leaf Size vs RMSE')
    plt.legend()
    plt.savefig('fig4.png')
    plt.gcf().clear()
