"""  		   	  			    		  		  		    	 		 		   		 		  
A simple wrapper for linear regression.  (c) 2015 Tucker Balch  		   	  			    		  		  		    	 		 		   		 		  
  		   	  			    		  		  		    	 		 		   		 		  
Copyright 2018, Georgia Institute of Technology (Georgia Tech)  		   	  			    		  		  		    	 		 		   		 		  
Atlanta, Georgia 30332  		   	  			    		  		  		    	 		 		   		 		  
All Rights Reserved  		   	  			    		  		  		    	 		 		   		 		  
  		   	  			    		  		  		    	 		 		   		 		  
Template code for CS 4646/7646  		   	  			    		  		  		    	 		 		   		 		  
  		   	  			    		  		  		    	 		 		   		 		  
Georgia Tech asserts copyright ownership of this template and all derivative  		   	  			    		  		  		    	 		 		   		 		  
works, including solutions to the projects assigned in this course. Students  		   	  			    		  		  		    	 		 		   		 		  
and other users of this template code are advised not to share it with others  		   	  			    		  		  		    	 		 		   		 		  
or to make it available on publicly viewable websites including repositories  		   	  			    		  		  		    	 		 		   		 		  
such as github and gitlab.  This copyright statement should not be removed  		   	  			    		  		  		    	 		 		   		 		  
or edited.  		   	  			    		  		  		    	 		 		   		 		  
  		   	  			    		  		  		    	 		 		   		 		  
We do grant permission to share solutions privately with non-students such  		   	  			    		  		  		    	 		 		   		 		  
as potential employers. However, sharing with other current or future  		   	  			    		  		  		    	 		 		   		 		  
students of CS 7646 is prohibited and subject to being investigated as a  		   	  			    		  		  		    	 		 		   		 		  
GT honor code violation.  		   	  			    		  		  		    	 		 		   		 		  
  		   	  			    		  		  		    	 		 		   		 		  
-----do not edit anything above this line---  		   	  			    		  		  		    	 		 		   		 		  
"""  		   	  			    		  		  		    	 		 		   		 		  
  		   	  			    		  	

import numpy as np
from random import randint

class RTLearner(object):

    def __init__(self, leaf_size = 1, verbose=False):
        #create tree variable to store tree
        self.tree = None
        self.leaf_size = leaf_size
        self.verbose = verbose

    
    def author(self):
        return 'akim376' # replace tb34 with your Georgia Tech username

        
    def build_tree(self, data):
        #convert data to dataX and dataY
        dataX = data.shape[1] - 1
        dataY = data[:, data.shape[1] - 1]
        
        if data.shape[0] <= self.leaf_size:
            return np.array([[-1, np.mean(data[:, -1]), -1, -1]]) #assign -1 for leaf
        elif np.all(data[0, -1] == data[:, -1], axis = 0):
            return np.array([[-1, data[0, -1], -1, -1]])
        else:
            
            best_feat_i = randint(0, data.shape[1] - 2)
            
            split_val = np.median(data[:, best_feat_i])

            maximum = max(data[:, best_feat_i])
            # then the right_tree is empty
            if maximum == split_val:
                return np.array([[-1, np.mean(data[:,-1]), -1, -1]])

            left_tree = self.build_tree(data[data[:,best_feat_i] <= split_val])
            right_tree = self.build_tree(data[data[:,best_feat_i] > split_val])

            root = np.array([[best_feat_i, split_val, 1, left_tree.shape[0] + 1]])
            #create left_array to store left side of the tree
            left_array = np.append(root, left_tree, axis = 0)
            #return the whole tree
            return np.append(left_array, right_tree, axis = 0)    
        
    
    def addEvidence(self,dataX,dataY):
        """  		   	  			    		  		  		    	 		 		   		 		  
        @summary: Add training data to learner  		   	  			    		  		  		    	 		 		   		 		  
        @param dataX: X values of data to add  		   	  			    		  		  		    	 		 		   		 		  
        @param dataY: the Y training values  		   	  			    		  		  		    	 		 		   		 		  
        """  	
        dataY=np.array([dataY])
        #tranpose the dataset to make it vertical
        dataY=dataY.T
        data=np.append(dataX,dataY,axis=1)
        self.tree=self.build_tree(data)

    
    def search_tree(self, points):
        i = 0
        #while not leaf
        while(self.tree[i,0] != -1):
            feat = self.tree[i, 0]
            split_val = self.tree[i, 1]

            if points[int(float(feat))] <= float(split_val):
                i = i + int(float(self.tree[i, 2]))
            else:
                i = i + int(float(self.tree[i, 3]))
        return self.tree[i,1]
  

    def query(self,points):
        """  		   	  			    		  		  		    	 		 		   		 		  
        @summary: Estimate a set of test points given the model we built.  		   	  			    		  		  		    	 		 		   		 		  
        @param points: should be a numpy array with each row corresponding to a specific query.  		   	  			    		  		  		    	 		 		   		 		  
        @returns the estimated values according to the saved model.  		   	  			    		  		  		    	 		 		   		 		  
        """        
        estimated_values = []
        num_row = points.shape[0]
        for i in range(0, num_row):
            value = self.search_tree(points[i, :])
            estimated_values.append(float(value))
        return estimated_values
        
if __name__=="__main__":  		   	  			    		  		  		    	 		 		   		 		  
    print "the secret clue is 'zzyzx'"
