"""
Template for implementing QLearner  (c) 2015 Tucker Balch

Copyright 2018, Georgia Institute of Technology (Georgia Tech)
Atlanta, Georgia 30332
All Rights Reserved

Template code for CS 4646/7646

Georgia Tech asserts copyright ownership of this template and all derivative
works, including solutions to the projects assigned in this course. Students
and other users of this template code are advised not to share it with others
or to make it available on publicly viewable websites including repositories
such as github and gitlab.  This copyright statement should not be removed
or edited.

We do grant permission to share solutions privately with non-students such
as potential employers. However, sharing with other current or future
students of CS 7646 is prohibited and subject to being investigated as a
GT honor code violation.

-----do not edit anything above this line---

Student Name: Tucker Balch (replace with your name)
GT User ID: akim376 (replace with your User ID)
GT ID: 903389023 (replace with your GT ID)
"""

import numpy as np
import random as rand

class QLearner(object):

    def author(self):
        return 'akim376' # replace tb34 with your Georgia Tech username.

    def __init__(self, \
        num_states=100, \
        num_actions = 4, \
        alpha = 0.2, \
        gamma = 0.9, \
        rar = 0.5, \
        radr = 0.99, \
        dyna = 0, \
        verbose = False):

        self.verbose = verbose
        self.num_actions = num_actions
        self.num_states = num_states
        self.alpha = alpha
        self.gamma = gamma
        self.rar = rar
        self.radr = radr
        self.dyna = dyna
        self.s = 0
        self.a = 0
        self.Q = np.random.uniform(-1,1,(num_states,num_actions))
        self.memory = []

    def querysetstate(self, s):
        """
        @summary: Update the state without updating the Q-table
        @param s: The new state
        @returns: The selected action
        """
        self.s = s

        if rand.random() < self.rar:
            action = rand.randint(0, self.num_actions-1)
        else:
            action = np.argmax(self.Q[self.s])

        if self.verbose: print "s =", s,"a =",action
        return action

    def query(self,s_prime,r):
        """
        @summary: Update the Q table and return an action
        @param s_prime: The new state
        @param r: The ne state
        @returns: The selected action
        """

        self.Q[self.s,self.a] = (1 - self.alpha) * self.Q[self.s,self.a] + self.alpha * (r + self.gamma * self.Q[s_prime,np.argmax(self.Q[s_prime])])
        self.memory.append([self.s, self.a, s_prime, r])

        if rand.random() < self.rar:
            action = rand.randint(0, self.num_actions-1)
        else:
            action = np.argmax(self.Q[s_prime])


        if self.dyna != 0:
            memory_rands = np.random.choice(len(self.memory), size = self.dyna, replace = True)
            for memory_rand in memory_rands:
                dyna_state, dyna_action, state_p, R = self.memory[memory_rand]
                self.Q[dyna_state,dyna_action] = (1 - self.alpha) * self.Q[dyna_state,dyna_action] + self.alpha * (R + self.gamma * self.Q[state_p,np.argmax(self.Q[state_p])])

        if self.verbose: print "s =", s_prime,"a =",action,"r =",r
        self.rar = self.rar * self.radr
        self.s = s_prime
        self.a = action
        return action

if __name__=="__main__":
    print "Remember Q from Star Trek? Well, this isn't him"
